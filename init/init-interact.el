(if (equal using-helm-or-ido 'ido)
    (require 'init-ido)
    (require 'init-helm))

(provide 'init-interact)
