(defvar my-name "Danny Gratzer")
(defvar my-email "gratzer@cs.au.dk")
(defvar my-smtp-server "danny.gratzer@gmail.com")
(defvar my-gpg-key "AAE62FAE")
(defvar my-redprl-location "~/jonprl/red/emacs/")

(provide 'init-personal)
